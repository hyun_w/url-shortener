import string
import random

CHARACTERS = string.ascii_letters + string.digits
BASE_LENGTH = len(CHARACTERS)


def encode_url(url):
    random_number = random.randrange(1, 9999999999999)
    return encode_number(sum([ord(_) for _ in url]) + random_number)


def encode_number(number):
    if number == 0:
        return CHARACTERS[0]

    arr = []
    while number:
        number, remain = divmod(number, len(CHARACTERS))
        arr.append(CHARACTERS[remain])
        arr.reverse()
    return ''.join(arr)
