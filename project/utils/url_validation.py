import re


def is_invalid_url_format(url: str) -> bool:
    '''
    url must contain http(s):// or ftp(s)://
    :param url: 'https://styleshare.kr?test=123'
    :return: bool
    '''
    ul = '\u00a1-\uffff'
    ip_regex = r'(?:25[0-5]|2[0-4]\d|[0-1]?\d?\d)(?:\.(?:25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}'
    hostname_regex = r'[a-z' + ul + r'0-9](?:[a-z' + ul + r'0-9-]{0,61}[a-z' + ul + r'0-9])?'
    domain_regex = r'(?:\.(?!-)[a-z' + ul + r'0-9-]{1,63}(?<!-))*'
    tld_regex = (
        r'\.'
        r'(?!-)'
        r'(?:[a-z' + ul + '-]{2,63}'
        r'|xn--[a-z0-9]{1,59})'
        r'(?<!-)'
        r'\.?'
    )

    host_regex = f'({hostname_regex}{domain_regex}{tld_regex}|localhost)'
    regex = re.compile(
        r'^(?:[a-z0-9\.\-\+]*)://'
        r'(?:\S+(?::\S*)?@)?'
        r'(?:' + ip_regex + '|' + host_regex + ')'
        r'(?::\d{2,5})?'
        r'(?:[/?#][^\s]*)?'
        r'\Z', re.IGNORECASE
    )

    if url.split('://')[0].lower() not in ['http', 'https', 'ftp', 'ftps']:
        return True
    if not regex.search(url):
        return True
    return False


def has_invalid_short_url_character(short_url: str) -> bool:
    return bool(re.search(r'[^a-zA-Z0-9-]', short_url))
