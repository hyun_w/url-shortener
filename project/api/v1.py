from flask import Blueprint, request, jsonify, redirect, abort

from project.models import UrlMap
from project.forms import UrlMapForm
from project.database import ro_session

api_v1 = Blueprint('', __name__)


@api_v1.route('/url', methods=['POST'])
def create_short_url():
    form = UrlMapForm.from_json(request.json)
    if not form.validate():
        return jsonify({'error': form.errors}), 400

    new_url_map = form.save()
    return jsonify({'data': new_url_map.serialize()}), 201


@api_v1.route('/<string:short_url>', methods=['GET'])
def redirect_origin_url(short_url):
    url_map = ro_session.query(UrlMap).filter(UrlMap.short_url == short_url).first()
    if not url_map:
        return abort(404)
    return redirect(url_map.url)
