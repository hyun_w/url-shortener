import re


class BaseError(Exception):
    message = ''

    def __init__(self, *args):
        self.args = args

    @property
    def slug(self):
        str1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', self.__class__.__name__)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', str1).upper()

    @property
    def description(self):
        return self.message.format(*self.args)

    def to_dict(self):
        return {'code': self.slug, 'message': self.description}


class OmitParameters(BaseError):
    message = '{} is required field'


class InvalidUrl(BaseError):
    message = '"{}" is not url form'


class InvalidShortUrl(BaseError):
    message = '"{}" is invalid short_url'


class DataTooLong(BaseError):
    message = '"{}" length must be less than {}'


class DataAlreadyExists(BaseError):
    message = '"{}" already exist'
