from flask import has_request_context, request, current_app
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import QueuePool
from werkzeug.local import LocalProxy

Base = declarative_base()
READ = 'READ'
READ_WRITE = 'WRITE'


class DB:
    def __init__(self, app):
        self.app = app
        app.db = self
        self.engine = self.get_engine(db_type=READ_WRITE)
        self.ro_engine = self.get_engine(db_type=READ)
        self.session = sessionmaker(autocommit=False, autoflush=False, bind=self.engine)
        self.ro_session = sessionmaker(autocommit=False, autoflush=False, bind=self.engine)
        self.app.teardown_request(close_session)

    def get_engine(self, db_type=READ_WRITE):
        uri = self.app.config['SQLALCHEMY_URI']
        if db_type == READ:
            uri = self.app.config.get('SQLALCHEMY_READ_URI', uri)

        engine = create_engine(
            uri,
            convert_unicode=True,
            poolclass=QueuePool,
            pool_size=self.app.config['POOL_SIZE'],
            pool_timeout=self.app.config['POOL_TIMEOUT'],
            max_overflow=self.app.config['MAX_OVERFLOW'],
            pool_recycle=self.app.config['POOL_RECYCLE']
        )
        return engine

    def get_session(self, db_type=READ_WRITE):
        if db_type == READ:
            return self.ro_session()
        return self.session()


@LocalProxy
def session():
    if not has_request_context():
        return

    context = request._get_current_object()
    if hasattr(context, '_session'):
        return context._session

    session_ = current_app.db.get_session(db_type=READ_WRITE)
    context._session = session_
    return session_


@LocalProxy
def ro_session():
    if not has_request_context():
        return

    context = request._get_current_object()
    if hasattr(context, '_ro_session'):
        return context._ro_session

    ro_session_ = current_app.db.get_session(db_type=READ)
    context._ro_session = ro_session_
    return ro_session_


def close_session(exception=None):
    if not has_request_context():
        return

    context = request._get_current_object()
    for attr in ['_session', '_ro_session']:
        session_ = getattr(context, attr, None)
        if not session_:
            continue
        if exception is not None:
            session_.rollback()
        session_.close()
        delattr(context, attr)
