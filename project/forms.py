import uuid

from wtforms import Form
from wtforms.fields import StringField

from project.models import UrlMap
from project.database import session
from project.utils.url_validation import has_invalid_short_url_character, is_invalid_url_format
from project.exceptions import BaseError, OmitParameters, InvalidUrl, InvalidShortUrl, DataAlreadyExists, DataTooLong


class UrlMapForm(Form):
    errors = None

    url = StringField()
    short_url = StringField()

    def validate(self):
        try:
            for name in self._fields:
                validator = getattr(self, f'validate_{name}', None)
                validator(getattr(self, name))
        except BaseError as e:
            self.errors = e.to_dict()
            return False
        return True

    def validate_url(self, field):
        if not field.data:
            raise OmitParameters('url')

        if len(field.data) > 500:
            raise DataTooLong(field.name, 500)

        if is_invalid_url_format(field.data):
            raise InvalidUrl(field.data)

    def validate_short_url(self, field):
        if not field.data:
            return

        if len(field.data) > 100:
            raise DataTooLong(field.name, 100)

        if has_invalid_short_url_character(field.data):
            raise InvalidShortUrl(field.data)

        if UrlMap.exists(short_url=field.data):
            raise DataAlreadyExists(field.data)

    def save(self):
        if not self.short_url.data:
            new_url_map = UrlMap.auto_generate(self.url.data)
        else:
            new_url_map = UrlMap(id=uuid.uuid4(), url=self.url.data, short_url=self.short_url.data)

        session.add(new_url_map)
        session.commit()
        return new_url_map
