import os
import wtforms_json

from dotenv import load_dotenv
from flask import Flask, jsonify

from .database import DB
from .api import api_v1

load_dotenv()


def create_app():
    app = Flask(__name__)
    wtforms_json.init()
    app.config.from_object(os.getenv('CONFIG_OBJECT', 'config.DevelopmentConfig'))
    DB(app)

    app.register_blueprint(api_v1, url_prefix='')

    @app.route('/health_check')
    def health_check():
        return jsonify(dict(ok='ok'))

    return app
