import uuid
import datetime

from sqlalchemy_utils import UUIDType
from sqlalchemy import Column, String, DateTime

from project.database import Base, session
from project.utils.base62 import CHARACTERS, encode_url


class UrlMap(Base):
    __tablename__ = 'url_map'

    id = Column(UUIDType, primary_key=True)
    url = Column(String(500), nullable=False)
    short_url = Column(String(100), nullable=False, unique=True)
    created_at = Column(DateTime, default=datetime.datetime.now())

    @classmethod
    def auto_generate(cls, url):
        while True:
            id_ = uuid.uuid4()
            short_url = encode_url(f'{url}')
            if len(short_url) > 100:
                continue
            if not cls.exists(short_url):
                break
        return UrlMap(id=id_, url=url, short_url=short_url)

    @classmethod
    def exists(cls, short_url):
        return bool(session.query(UrlMap).filter(UrlMap.short_url == short_url).scalar())

    def serialize(self):
        return {'url': self.url, 'short_url': self.short_url}
