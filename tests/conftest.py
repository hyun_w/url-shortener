import os
import uuid
import pytest

from sqlalchemy import create_engine

from project.database import Base
from project.app import create_app
from project.models import UrlMap

from config import TestConfig


@pytest.fixture(scope='session')
def db():
    split_uri = TestConfig.SQLALCHEMY_URI.split('/')
    test_uri = '/'.join(split_uri[:-1])
    db_name = split_uri[-1]

    tmp_engine = create_engine(test_uri)
    conn = tmp_engine.connect()
    conn.execute(f'DROP DATABASE IF EXISTS {db_name}')
    conn.execute(f"CREATE DATABASE {db_name} default character set utf8mb4")

    yield

    conn.execute(f'DROP DATABASE IF EXISTS {db_name}')
    conn.close()


@pytest.fixture(scope='session')
def app(db):
    os.environ['CONFIG_OBJECT'] = 'config.TestConfig'
    app_ = create_app()
    app_context = app_.app_context()
    app_context.push()

    engine = app_.db.get_engine()
    metadata = Base.metadata
    metadata.create_all(bind=engine)

    yield app_
    metadata.drop_all(bind=engine)
    engine.dispose()
    app_context.pop()


@pytest.fixture(scope='session')
def client(app):
    return app.test_client()


@pytest.fixture(scope='session')
def session(app):
    session_ = app.db.get_session()

    yield session_

    session_.rollback()
    session_.close()


@pytest.fixture(scope='function')
def url_data(session):
    url = UrlMap(id=uuid.uuid4(), url='https://google.com', short_url='styleshare')
    session.add(url)
    session.commit()
    yield
    session.query(UrlMap).delete()
    session.commit()
