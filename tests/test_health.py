def test_health(client):
    res = client.get('/health_check')
    assert res.status_code == 200
