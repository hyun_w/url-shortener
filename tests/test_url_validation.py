from project.utils.url_validation import is_invalid_url_format, has_invalid_short_url_character


def test_url_without_schema_fail():
    url = 'www.styleshare.kr'
    assert is_invalid_url_format(url)


def test_url_invalid_hostname_fail():
    url = 'https://te!st.com'
    assert is_invalid_url_format(url)


def test_url_invalid_domain_fail():
    url = 'https://styleshare.k@r'
    assert is_invalid_url_format(url)


def test_url_invalid_ipv4_fail():
    url = 'https://192.168.12.3213/index'
    assert is_invalid_url_format(url)

    url = 'https://192.168.12.a13/index'
    assert is_invalid_url_format(url)


def test_url_ipv4_success():
    url = 'https://192.168.12.33/#index'
    assert not is_invalid_url_format(url)


def test_url_with_port_success():
    url = 'https://styleshare.kr:3333/index'
    assert not is_invalid_url_format(url)


def test_url_with_query_success():
    url = 'http://styleshare.kr/index?aaa=123&bbb=3a3'
    assert not is_invalid_url_format(url)


def test_short_url_special_character_fail():
    short_url = 'a@1C!a'
    assert has_invalid_short_url_character(short_url)


def test_short_url_with_dash_success():
    short_url = 'styleshare-coding-test'
    assert not has_invalid_short_url_character(short_url)
