from project.exceptions import OmitParameters, InvalidShortUrl, InvalidUrl, DataAlreadyExists, DataTooLong


def test_short_url_not_found_fail(client, session):
    test_url = 'nothing'
    res = client.get(f'/{test_url}')

    assert res.status_code == 404


def test_creation_no_url_fail(client):
    res = client.post('/url')
    assert res.status_code == 400
    assert res.json['error'] == OmitParameters('url').to_dict()


def test_creation_invalid_url_fail(client):
    no_url = 'test.invalid.u@rl?a=test&b=te'
    res = client.post('/url', json={'url': no_url})
    assert res.status_code == 400
    assert res.json['error'] == InvalidUrl(no_url).to_dict()


def test_too_long_url_fail(client):
    no_url = 'https://styleshare.kr/' + 'a' * 500
    res = client.post('/url', json={'url': no_url})
    assert res.status_code == 400
    assert res.json['error'] == DataTooLong('url', 500).to_dict()


def test_creation_short_url_exists_fail(client, url_data, session):
    params = {'url': 'http://google.com/test-url', 'short_url': 'styleshare'}
    res = client.post('/url', json=params)

    assert res.status_code == 400
    assert res.json['error'] == DataAlreadyExists(params['short_url']).to_dict()


def test_creation_invalid_short_url_fail(client, session):
    params = {'url': 'http://google.com/test-url', 'short_url': 'style?share'}
    res = client.post('/url', json=params)

    assert res.status_code == 400
    assert res.json['error'] == InvalidShortUrl(params['short_url']).to_dict()


def test_creation_short_url_too_long_fail(client):
    short_url = 'a' * 101
    params = {'url': 'http://google.com/test-url', 'short_url': short_url}
    res = client.post('/url', json=params)

    assert res.status_code == 400
    assert res.json['error'] == DataTooLong('short_url', 100).to_dict()


def test_creation_auto_generate_url_success(client, session):
    params = {'url': 'http://google.com/test-url'}
    res = client.post('/url', json=params)

    assert res.status_code == 201
    assert bool(res.json['data']['short_url'])


def test_creation_with_name_success(client, session):
    url = 'http://google.com/test-url'
    short_url = 'styleshare-test'
    params = {'url': url, 'short_url': short_url}
    res = client.post('/url', json=params)

    assert res.status_code == 201
    assert res.json['data'] == {'url': url, 'short_url': short_url}
