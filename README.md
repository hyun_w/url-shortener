# URL SHORTHENER 

### Requirements
```bash
python >= 3.5
virtualenv
mysql
```

### Installation
```bash
$ git clone https://hyun_w@bitbucket.org/hyun_w/url-shortener.git 

```

```bash
Move project root and open .env file(for coding test, uploaded .env to git).
$ cd url-shortener
$ vi .env

Fix DB config to connect mysql.
DB_NAME=test
DB_USER=test
DB_PASSWORD=test
...

``` 

```bash
$ virtualenv .venv -ppython3
$ source ./.venv/bin/activate
$ pip install -r requirements.txt
$ alembic upgrade heads 
$ python start.py 
```

### For test
```bash
Move project root.
$ cd url-shortener
$ pytest 
```
