import os
import sys
from project.app import create_app

app = create_app()

if __name__ == '__main__':
    host = '0'
    port = 8001
    if len(sys.argv) > 1:
        host, port = sys.argv[1].split(':')
        port = int(port)
    os.environ['FLASK_ENV'] = os.getenv('FLASK_ENV') or 'development'
    app.run(host=host, port=port, debug=True)
