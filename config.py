import os


class Config:
    DEBUG = False
    TESTING = False

    POOL_SIZE = int(os.getenv('POOL_SIZE', 1))
    MAX_OVERFLOW = int(os.getenv('MAX_OVERFLOW', 2))
    POOL_TIMEOUT = int(os.getenv('POOL_TIMEOUT', 2))
    POOL_RECYCLE = int(os.getenv('POOL_RECYCLE', 900))

    SQLALCHEMY_URI = os.environ['SQLALCHEMY_URI']
    SQLALCHEMY_READ_URI = os.getenv('SQLALCHEMY_READ_URI', SQLALCHEMY_URI)


class ProductionConfig(Config):
    pass


class DevelopmentConfig(Config):
    DEBUG = True


class TestConfig(Config):
    TESTING = True

    SQLALCHEMY_URI = os.environ['SQLALCHEMY_TEST_URI']
    SQLALCHEMY_READ_URI = SQLALCHEMY_URI

